function vidcutout --description 'will cutout video between a specified start and end time'

    set -l input
    set -l cutstart
    set -l cutend
    set cdate (echo (date +%s))

    getopts $argv | while read -l key value
        switch $key
            case i input
                set input $value
            case s cutstart
                set cutstart $value
            case e cutend
                set cutend $value
            case h
                echo "vidcutout -i videofile.video -s 00:00:00 -e 00:00:00"
                return 0
            case \*
                printf "error: Unknown option %s\n" $option
        end
    end


    if set -q input[1]
        if set -q cutstart[1]
            if set -q cutend[1]
                echo 'confirmed video cutting'
                command ffmpeg -i $input -ss $cutstart -to $cutend -c copy $cdate.CUT.$input
            end
        end
    end





end