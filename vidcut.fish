function vidcut --description 'will cut video file at specified time'

    set -l input
    set -l cutat

     getopts $argv | while read -l key value
        switch $key
            case i input
                set input $value
            case c cutat
                set cutat $value
            case h
                echo "vidcut -i videofile.video -c 00:00:00"
                return 0
            case \*
                printf "error: Unknown option %s\n" $option
        end
    end


    if set -q input[1]
        if set -q cutat[1]
            echo 'confirmed video cutting'
            command ffmpeg -i $input -to $cutat -c copy LEFT.$input
            command ffmpeg -i $input -ss $cutat -c copy RIGHT.$input
        end
    end


end