function mvsub2cur --description 'moves all files in subdirectories to current '
	echo 'current location is at:' 
	command pwd
	if read_confirm
		echo 'confirmed'
		command find ./ -type f -print0 | xargs -0 mv -t ./
	end
end
